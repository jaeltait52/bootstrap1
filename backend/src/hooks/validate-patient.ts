// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';

export default (options = {}): Hook => {
  return async (context: HookContext) => {
    const { app, data } = context;
    
    const patients = await app.service('patients').find({
      query: {
        email: data.email
      }
    });
    
    // Throw an error if this is a duplicate plate
    if(patients.total > 0) {
      throw new Error('Duplicate email');
    } 
    
    return context;
  };
}