import React, { Component } from 'react';

class Patients extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      country: 'canada',
      provState: '',
      postal: '',
      lowestBP: '',
      highestBP: '',
      formclass: ''
    };
  
  this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
  this.handleLastNameChange = this.handleLastNameChange.bind(this);
  this.handleEmailChange = this.handleEmailChange.bind(this);
  this.handleCountryChange = this.handleCountryChange.bind(this);
  this.handleProvStateChange = this.handleProvStateChange.bind(this);
  this.handlePostalChange = this.handlePostalChange.bind(this);
  this.handleLowestBPChange = this.handleLowestBPChange.bind(this);
  this.handleHighestBPChange = this.handleHighestBPChange.bind(this);
  this.handleSubmit = this.handleSubmit.bind(this);
  }
  
  handleFirstNameChange(event) {
    this.setState({firstName: event.target.value});
  }
  
  handleLastNameChange(event) {
    this.setState({lastName: event.target.value});
  }
  
  handleEmailChange(event) {
    this.setState({email: event.target.value});
  }
  
  handleCountryChange(event) {
    this.setState({country: event.target.value});
  }
  
  handleProvStateChange(event) {
    this.setState({provState: event.target.value});
  }
  
  handlePostalChange(event) {
    this.setState({postal: event.target.value});
  }
  
  handleLowestBPChange(event) {
    this.setState({lowestBP: event.target.value});
  }
  
  handleHighestBPChange(event) {
    this.setState({highestBP: event.target.value});
  }
  
  deletePatient(id, ev) {
    // HERE IS YOUR HANDLE TO THE BACKEND PATient SERVICE
    const { patientService } = this.props;

    /////////////////////////////////////////////////
    // WRITE THE CODE HERE TO REMOVE THE SELECTED PATIENT
    // START v
    /////////////////////////////////////////////////
    patientService.remove(id);
    /////////////////////////////////////////////////
    // END ^
    /////////////////////////////////////////////////
  }
  
  handleSubmit(ev) {
    // HERE IS YOUR HANDLE TO THE BACKEND PATIENT SERVICE
    const { patientService } = this.props;
    
    ///////////////////////////////////////
    // WRITE THE CODE HERE TO ADD A NEW PATient
    // START v
    ///////////////////////////////////////

if (ev.target.checkValidity()) {
      patientService.create({
        firstName: this.state.firstName,
        lastName: this.state.lastName,
        email: this.state.email,
        country: this.state.country,
        provState: this.state.provState,
        postal: this.state.postal,
        lowestBP: this.state.lowestBP,
        highestBP: this.state.highestBP
      })
      .then(() => {
        console.log("Created new patient.")
        this.setState({
          firstName: '',
          lastName: '',
          email: '',
          country: 'canada',
          provState: '',
          postal: '',
          lowestBP: '',
          highestBP: '',
          formclass: ''
        });      
      })
      .catch(error => {
        this.setState({
          formclass: '',
        });
      });
    } else {
      this.setState({
        formclass: 'was-validated'
      });
    }


    ///////////////////////////////////////
    // END ^
    ///////////////////////////////////////
    
    ev.preventDefault();
  }

  render() {
    const { patients } = this.props;
    return(
    <div>
      <div className="py-5 text-center">
        <h2>Patients</h2>
      </div>

      <div className="row">
        <div className="col-md-12 order-md-1">
          <form onSubmit={this.handleSubmit} className={this.state.formclass} noValidate id="patientform">
            <div className="row">
              
              <div className="col-lg-3 col-md-6 mb-3">
                <label htmlFor="firstName">First Name</label>
                <input type="text" className="form-control" id="firstName"
                  value={this.state.firstName} required onChange={this.handleFirstNameChange} />
                <div className="invalid-feedback">
                    A first name is required.
                </div>
              </div>
              
              <div className="col-lg-3 col-md-6 mb-3">
                <label htmlFor="lastName">Last Name</label>
                <input type="text" className="form-control" id="lastName"
                  value={this.state.lastName} required onChange={this.handleLastNameChange} />
                <div className="invalid-feedback">
                    A last name is required.
                </div>
              </div>
              
              <div className="col-lg-3 col-md-6 mb-3">
                <label htmlFor="email">Email</label>
                <input type="email" className="form-control" id="email"
                  value={this.state.email} required onChange={this.handleEmailChange} />
                <div className="invalid-feedback">
                    A valid email is required.
                </div>
              </div>
              
              <div className="col-lg-3 col-md-3 mb-3">
                <label htmlFor="country">Country</label>
                <select className="form-control" required onChange={this.handleCountryChange}>
                  <option value="canada"> Canada </option>
                  <option value="usa"> USA </option> 
                </select>
              </div>
              
            {this.state.country === "canada" ? (
              <div className="col-lg-3 col-md-3 mb-3">
                <label htmlFor="provState">Province</label>
                <select className="form-control" required onChange={this.handleProvStateChange}>
                  <option value="ab"> Alberta </option>
                  <option value="bc"> British Columbia </option>
                  <option value="mn"> Manitoba </option>
                  <option value="nb"> New Brunswick </option>
                  <option value="nl"> Newfoundland and Labrador </option>
                  <option value="nwt"> Northwest Territories </option>
                  <option value="ns"> Nova Scotia </option>
                  <option value="nu"> Nunavut </option>
                  <option value="on"> Ontario </option>
                  <option value="pei"> PEI </option>
                  <option value="qu"> Quebec </option>
                  <option value="sk"> Saskatchewan </option>
                  <option value="yk"> Yukon </option>
                </select>
                </div>
              ) : (
              <div className="col-lg-3 col-md-3 mb-3">
                <label htmlFor="provState">State</label>
                <select className="form-control" required onChange={this.handleProvStateChange}>
                  <option value="s1"> State1 </option>
                  <option value="s2"> State2 </option>
                </select>
                </div>
              )}
              
            {this.state.country === "canada" ? (
              <div className="col-lg-3 col-md-6 mb-3">
                <label htmlFor="postal">Postal Code</label>
                <input type="text" className="form-control" id="postal" pattern="[A-Za-z][0-9][A-Za-z] [0-9][A-Za-z][0-9]"
                  value={this.state.postal} required onChange={this.handlePostalChange} />
                <div className="invalid-feedback">
                    A postal code is required.
                </div>
              </div>
              ) : (
              <div className="col-lg-3 col-md-6 mb-3">
                <label htmlFor="postal">ZIP</label>
                <input type="text" className="form-control" id="postal" pattern="(\d{5}([\-]\d{4})?)"
                  value={this.state.postal} required onChange={this.handlePostalChange} />
                <div className="invalid-feedback">
                    A ZIP code is required.
                </div>
              </div>
              )}

              
              <div className="col-lg-3 col-md-6 mb-3">
                <label htmlFor="lowestBP">Lowest systolic blood pressure</label>
                <input type="number" className="form-control" id="lowestBP" min="80" max="180"
                  value={this.state.lowestBP} required onChange={this.handleLowestBPChange} />
                <div className="invalid-feedback">
                    A lowest systolic blood pressure is required.
                </div>
              </div>
              
              <div className="col-lg-3 col-md-6 mb-3">
                <label htmlFor="highestBP">Highest systolic blood pressure</label>
                <input type="number" className="form-control" id="highestBP" min={this.state.lowestBP} max="180"
                  value={this.state.highestBP} required onChange={this.handleHighestBPChange} />
                <div className="invalid-feedback">
                    A highest systolic blood pressure is required.
                </div>
              </div>
            
              
            </div>
            <button className="btn btn-primary btn-lg btn-block" type="submit">Add patient</button>
          </form>
        </div>
      </div>
      
      <table className="table">
        
      </table>

      <footer className="my-5 pt-5 text-muted text-center text-small">
        <p className="mb-1">&copy; 2020 CPSC 2650</p>
      </footer>
    </div>
    );
  }
}

export default Patients;
